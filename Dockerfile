# NOTE: This Dockerfile is used to deploy CVMicroservice
#
# NOTE: It's based on alpine image instead of alpine/git
#       I prefered this official image over the alpine/git
#   
FROM alpine

# JAVA_HOME, M2_HOME and other env will be set here
ENV JAVA_HOME /usr/lib/jvm/default-jvm/

# Install tini and bash
RUN apk add --update tini && \
    apk add --no-cache bash

# Install git and openssh
RUN apk --update add git less openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

# Install JDK 8
RUN apk update
RUN apk fetch openjdk8
RUN apk add openjdk8

# Install maven
RUN apk fetch maven
RUN apk add maven

# SSH Config
RUN mkdir /root/.ssh && \
    echo $'Host gitlab.com\n\tHostname gitlab.com\n\tIdentityFile /root/.ssh/id_rsa_gitlab\n\tPreferredauthentications publickey' >> /etc/ssh/ssh_config

# Copy over private key, and set permissions
# Warning! Anyone who gets their hands on this image will be able
# to retrieve this private key file from the corresponding image layer
ADD id_rsa_gitlab /root/.ssh/id_rsa_gitlab

RUN chmod 400 /root/.ssh/id_rsa_gitlab

# Create known_hosts
RUN touch /root/.ssh/known_hosts

# Add gitlab key
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

#RUN ln -sf /dev/stdout /var/log/nginx/access.log \
#	&& ln -sf /dev/stderr /var/log/nginx/error.log
# forward request and error logs to docker log collector

# Clone the conf files into the docker container
RUN git clone git@gitlab.com:YounesNAJA/CVMicroservice.git

EXPOSE 8080
# expose these ports on the docker virtual network
# you still need to use -p or -P to open/forward these ports on host

RUN cd /CVMicroservice && mvn package
RUN cp /CVMicroservice/target/CVMicroservice-0.0.1-SNAPSHOT.jar /
RUN rm -rf /CVMicroservice

ENTRYPOINT ["java","-jar", "/CVMicroservice-0.0.1-SNAPSHOT.jar"]
# CMD ["/bin/bash"]
# required: run this command when container is launched
# only one CMD allowed, so if there are multiple, last one wins